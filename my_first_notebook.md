My first R lab notebook
================
Joanes Grandjean

## Table of content

1.  [Getting started](#getting-started)
2.  [Why use R and git](#why-use-r-and-git)
3.  [Lab journal introduction](#lab-journal-introduction)
4.  [My first R code](#my-first-r-code)
5.  [Exercise](#exercise)
6.  [Version control](#version-control)

### Getting started

Open R studio, go to ‘file \> new project’, select ‘version control’,
and ‘git’. Paste the following as your git URL:
‘<https://gitlab.socsci.ru.nl/grandjeanlab/rstudio_training.git>’ and
click ‘create project’.

If git is not installed, you will have an error message. [How to install
git for
windows](https://github.com/git-for-windows/git/releases/download/v2.25.0.windows.1/Git-2.25.0-32-bit.exe)
[How to install git for
mac](https://sourceforge.net/projects/git-osx-installer/files/git-2.23.0-intel-universal-mavericks.dmg/download?use_mirror=autoselect)

If git is installed, R studio will download (aka ‘pull’) the code from
the gitlab website, and open it into a new project which you can edit.

### Why use R and git

![](assets/img/phd101212s.gif)

*(from phdcomics.com)*

R is a powerful software for statistical analysis. It allows for simple
table manipulations, and graph plots. In addition to the basic software,
it also comes with several dedicated packages which enhances specific
aspects, such as plotting with ggplot2 (grammar of graphic), or linear
mixed models with lme4.

Modern installments of R, through the graphical user interface R studio
allow to keep R notebooks which combine text (and simple formatting
rules =\> [markdown](https://en.wikipedia.org/wiki/Markdown)), with
traditional codes. This is what we are using now. This section is part
of the text. Code examples will follow below. Interestingly, R notebook
can integrate code from multiple languages into one notebook (one
notebook to rule them all). For instance, it can run `bash` code,
`python`, and of course, `r`.

In this exercise, we will see how a R notebook is kept, how to do basic
text formatting, how to integrate `r` code into the document, and how to
output this into a document that can be shared (e.g. html, pdf, md).
Finally we will see how Rstudio integrates `git` for version control,
project management, and collaborative work.

### Lab journal introduction

The idea being a lab journal is to keep track of protocols, analysis,
results, and to provide a short interpretation of the results. This
allows the supervisor to keep track of the progress, it also allows the
student to have a track-record of previous experiments and what how
protocols and analysis evolved over time.

The R notebook format allows adding images to the notebook, text
comments, but also, code. By using git, the student ensures that the lab
journal is archived and accessible for later use (e.g. by future
students, etc..)

Fundamentally, your lab journal can be organized in two ways, either
chronologically, or following a narrative.

#### Chronological journal example.

##### 07.02.2020

Today I learnt to use an electrical lab journal.

##### 08.02.2020

Today, I implemented a `R` function into my lab journal

``` r
print('hello from R')
```

    ## [1] "hello from R"

I’ve learnt that I can implement codes in my lab notebook. I’ve learn
that the `print` function returns the argument as a line of text. Using
the little green arrow on the top right corner of the R code section, I
can run the code above. A section of code within the text is known as a
‘chunk’

#### Narrative lab journal

##### Intro

I seek to learn how to use R and R notebooks

##### Results

``` r
a<-'my results'
print(a)
```

    ## [1] "my results"

above are the results of my second line of code. I’ve learnt that I can
put text into a variable `a` and use the `print` function to display the
content of that variable. A text variable is known as a `string` (for: a
string of characters)

##### Conclusion

I can present my lab notebook either chronologically or as a narrative.
If I use a narrative format, I can anyway retrieve the older versions of
the document on the git repository (if I’ve uploaded, i.e. pushed, the
codes on a regular basis).

#### previewing the text

If the document is opened in R, you are currently looking at the editor,
which is used to write code, etc.. To get a previous of the document
neatly organized, press the ‘knit’ button. A new window should open with
the formatted document. ‘Knitting’ effectively assembles the document
together, and applies different formatting options that you may have
set.

#### How to format the text

The text section of a R notebook is formatted using ‘markdown’. You can
find [cheat
sheets](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
into markdown on the internet.

Briefly, you can format text into header with `#`’s.

# One ‘\#’ for larger header (H1)

## Two ‘\#’ for smaller header (H2)

### Three ‘\#’ for smaller header (H3), etc…

You can also make text *italic* using one `*` or `_`, or **bold** using
`**` or `__`. You can combine both ***bold italic*** using `**_`

Make bullet points with `*`

  - first bullet
  - second bullet
  - etc

Or you can make lists using `1.`

1.  point uno
2.  point duo

You can add images to your documents. Presently, the document shows an
image found in `assets/img/opto-1.png`.

![a fMRI image](assets/img/opto-1.png)

*Figure 1 | My first image*

So now we have looked into the text part of the electronic journal.
Let’s see how we can embed some code.

### My first R code

#### The print function

R is a relatively simple coding language. Let us give the first
instruction. Below is the first code. We’ve seen it previously. It is
contained in a ‘chunk’. Notice the 3 backward quotes and the `{r}`. This
tells the program that it is now looking at R code. You can add new
chunks by pressing `Ctl+Alt+i`, or pressing the ‘insert’ button at the
top of this document.

``` r
print('hello world')
```

    ## [1] "hello world"

One can run the content of the cell above by pressing into the green
arrow in the top right corner of the chunk, or when clicking on the
chunk, pressing `Ctl+shift+enter`. You can run one line of the chunk at
the time using `Ctl+enter`. The output is a line `[1] "hello world"`

The command ‘print’ is a function used to instruct the program to
display text. The text within the parenthesis is called the argument.
Presently, this is a string (i.e. text).

##### variables. Numbers

We have seen the first function above. Now let us learn to use
variables. In R, we give a variable a value using the arrow `<-`. A
variable can have any name but must start with a letter, and shouldn’t
share the same name as a function. For instance, don’t use `print` as a
name for your variable (e.g. `print<-1`). This would confuse the
program.

``` r
a<-1
b<-2
a+b
```

    ## [1] 3

Above, we attribute the value ‘1’ (as a number) to variable ‘a’, and ‘2’
to ‘b’. We can add them using the `+` operator.

#### Variables. String

R makes the difference between numeric variable or character variables
(known as strings). Character variables are given with quotation marks.
The commands below return an error because it is not possible to add two
strings using the `+` operator.

``` r
a<-'1'
b<-'2'
#a+b
```

Instead, we can use the `paste` function. This concatenates two or more
string. It requires a separator `sep=` argument to detail how the two
strings are meant to be separated.

``` r
a<-'1'
b<-'2'

paste(a,b,sep='')
```

    ## [1] "12"

#### Variable. Vectors

A vector is a ensemble of elements within a variable. They can be
initiated using the ‘c()’ format.

``` r
c<-c(1,2,3,4)

print(c)
```

    ## [1] 1 2 3 4

Vectors can also be strings (notice the quotations).

``` r
d<-c('a','b','c')
print(d)
```

    ## [1] "a" "b" "c"

Vectors can be combined in multiple ways. Strict addition

``` r
e<-c(1,2,3,4)
f<-c(4,5,8,4)
e+f
```

    ## [1]  5  7 11  8

Concatenation along its length

``` r
c(e,f)
```

    ## [1] 1 2 3 4 4 5 8 4

Concatenation intro different columns

``` r
cbind(e,f)
```

    ##      e f
    ## [1,] 1 4
    ## [2,] 2 5
    ## [3,] 3 8
    ## [4,] 4 4

Concatenation into different rows

``` r
rbind(e,f)
```

    ##   [,1] [,2] [,3] [,4]
    ## e    1    2    3    4
    ## f    4    5    8    4

### Exercise

Create a chunk of code in R. Create a vector of 6 numbers. Create a
‘for’ loop that `print` the content of the vector for each element
one after the other. Remember, when coding, [google is your
friend](https://lmgtfy.com/?q=r+for+loop)

### Version control

Congratulation. You’ve gone to the end of this tutorial. Importantly,
you want to keep a version of your code.

  - so that you can retrieve it
  - so that other people can retrieve it
  - so that other people can work with you on it.

On the top right side there should be in R studio, a panel with
‘Environment’, ‘History’, ‘Connections’, ‘Git’. Go to ‘Git’

We want to crease a new branch where your code can exist without
interfering with the main code (known as the ‘master’). Click on ‘New
Branch’ and name it after you ‘Student\#1’.

Now go to ‘Commit’, this is where you can select files that you have
modified. ‘my\_first\_notebook.Rmd’ should have a small blue ‘m’ icon in
front of it to indicate you have modified it.

You can click on the file and see in the editor below in green and red
what was added and what was removed from the code, respectively.

Make sure there is a tick on the left side of the ‘m’ icon. This means
it is selected to be uploaded into your ‘branch’. Each time you prepare
to upload some code, you need to provide a text justification, usually
to keep track of the changes. e.g. ‘fixed bug for issue \#2’.

Now that you committed your code, you can push it to the repository by
clicking on he green arrow ‘Push’. Your code should now appear on the
\[gitlab repository\]
(<https://gitlab.socsci.ru.nl/grandjeanlab/rstudio_training.git>)

Congratulations\!\!
